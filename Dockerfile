FROM centos:centos7
MAINTAINER "santy"

ARG role

ADD  mongodb-org-3.2.repo /etc/yum.repos.d/
RUN yum install -y mongodb-org

RUN mkdir -p /data/db
RUN mkdir -p /var/log/mongo/db
RUN touch /var/log/mongo/db/db.log

ADD $role /data/db/

#EXPOSE 27017 
#ENTRYPOINT ["/usr/bin/mongod", "--config", "/data/]
