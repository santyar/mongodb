**Create mongo sharded cluster infrastructure**

1.Install docker and docker-compose

2.Download all files from repo in some folder

3.Go in to this folder and do command ***docker-compose up -d*** 

after run this command will be create 13 docker container

4.You can check it with command ***docker-compose ps***

 5.Now we must create 3 Replica set cluster for Sharded
we can do this with command

***mongo --port 3007 --eval "rs.initied({ "_id": "rs0", members : [ {"_id" : 0, host : "mongo_rs0_master:27017"}, {"_id" : 1, host : "mongo_rs0_secondary:27017"}, {"_id" : 2, host : "mongo_rs0_arbitr",arbiterOnly : true} ] });"***

***mongo --port 3007 --eval "rs.initied({ "_id": "rs1", members : [ {"_id" : 0, host : "mongo_rs1_master:27017"}, {"_id" : 1, host : "mongo_rs1_secondary:27017"}, {"_id" : 2, host : "mongo_rs1_arbitr",arbiterOnly : true} ] });"***

***mongo --port 3007 --eval "rs.initied({ "_id": "rs2", members : [ {"_id" : 0, host : "mongo_rs2_master:27017"}, {"_id" : 1, host : "mongo_rs2_secondary:27017"}, {"_id" : 2, host : "mongo_rs2_arbitr",arbiterOnly : true} ] });"****


6. Than login in to mongos.

***mongo --port 3019***

and make command

***sh.addShard( "rs0/mongo_rs0_master:27017,mongo_rs0_secondary:27017" )***

***sh.addShard( "rs1/mongo_rs1_master:27017,mongo_rs1_secondary:27017" )***

***sh.addShard( "rs2/mongo_rs2_master:27017,mongo_rs2_secondary:27017" )***

than you can make 

***sh.status()*** and see 

as you can see we don't have any db on cluster but balancer already ebabled.

7.You can download test db with name companies in this cluster

***mongoimport --db companies --file ./companies.json*** 

after download db we can make

***sh.enableSharding("companies")***

8. We must enable sharded to the collection

we must create or use standart indexes for sahrded

we will use standart indexes "_id"

***use companies***
**
*db.companies.ensureIndex({"_id":1})***

***sh.shardCollection("companies.companies", {"_id":1})***

than we can** *make sh.status()***
and see that balancer is enabled and sharded or collections enabled too